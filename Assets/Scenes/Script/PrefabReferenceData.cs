﻿using UnityEngine;
 
namespace Bloodthirst.Core.BISDSystem
{
    [CreateAssetMenu(menuName = "ReferencePrefab")]
    public class PrefabReferenceData : ScriptableObject
    {
        [SerializeField]
        private SmartPrefab prefabReference;
 
        public SmartPrefab PrefabReference => prefabReference;
    }
}
