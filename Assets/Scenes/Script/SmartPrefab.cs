﻿using System.Collections;
using System.Collections.Generic;
using Bloodthirst.Core.BISDSystem;
using UnityEngine;

public class SmartPrefab : MonoBehaviour
{
    [SerializeField]
    private PrefabReferenceData prefabReferenceData;
    
    void Start()
    {
        Debug.Log("Hijos del prefab avezado: " + transform.childCount);
        StartCoroutine(Proliferate());
    }

    IEnumerator Proliferate()
    {
        new GameObject("SmartChild").transform.parent = transform;
        yield return new WaitForSeconds(1f);
        SmartPrefab smartPrefab = Instantiate(prefabReferenceData.PrefabReference);
        smartPrefab.transform.position = transform.position + Vector3.up;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject,.5f);
    }

}
