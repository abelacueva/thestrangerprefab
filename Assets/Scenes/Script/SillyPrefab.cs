﻿using System;
using System.Collections;
using Bloodthirst.Core.BISDSystem;
using UnityEditor;
using UnityEngine;

public class SillyPrefab : MonoBehaviour
{
    [SerializeField] private SillyPrefab _sillyPrefab;
    
    void Start()
    {
        Debug.Log("Hijos del prefab estúpido: " + transform.childCount);
        StartCoroutine(Proliferate());
    }

    IEnumerator Proliferate()
    {
        new GameObject("StupidChild").transform.parent = transform;
        yield return new WaitForSeconds(2f);
        SillyPrefab sillyPrefab = Instantiate(_sillyPrefab);
        sillyPrefab.transform.position = transform.position + Vector3.down;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject,.5f);
    }
    
}